$(function() {
	var slideout = new Slideout({
		'panel': document.getElementById('panel'),
		'menu': document.getElementById('menu'),
		'padding': 256,
		'tolerance': 70
	});
	$('#panel #header').on('click', function() {
		slideout.toggle();
	});

});

function checkLogin($rootScope,$username,$location,redirect,redirectTo){
	if ($username === undefined) {
		if(redirect === true){
			$location.path("/");
		}
	} else {
		$rootScope.loginSuccess = true;
		$location.path(redirectTo);
	}	
}
//angular code
var app = angular.module("onvif", ["ngRoute", "ngStorage"]);

app.directive('loading', ['$http', function($http) {
	return {
		restrict: 'A',
		link: function(scope, elm, attrs) {
			scope.isLoading = function() {
				return $http.pendingRequests.length > 0;
			};
			scope.$watch(scope.isLoading, function(v) {
				if (v) {
					elm.show();
				} else {
					elm.hide();
				}
			});
		}
	};
}]);
app.directive('backButton', function() {
	return {
		restrict: 'A',

		link: function(scope, element, attrs) {
			element.bind('click', goBack);

			function goBack() {
				history.back();
				scope.$apply();
			}
		}
	}
});
app.config(function($routeProvider, $locationProvider) {
	$routeProvider
		.when('/', {
			templateUrl: 'login.html',
			controller: 'loginCtrl'
		})
		.when('/addcam', {
			templateUrl: 'addcam.html',
			controller: 'scanCtrl'
		})
		.when('/register', {
			templateUrl: 'register.html',
			controller: 'regCtrl'
		})
		.when('/forgotpass', {
			templateUrl: 'forgotpass.html',
			controller: 'fpCtrl'
		})
		.when('/mycameras', {
			templateUrl: 'mycameras.html',
			controller: 'camCtrl'
		})
		.otherwise({
			redirectTo: '/addcam'
		});	
});

app.controller("loginCtrl", ["$scope", "$rootScope", "$http", "$sessionStorage", "$location", "$timeout", function($scope, $rootScope, $http, $sessionStorage, $location, $timeout) {
	$scope.$storage = $sessionStorage;
	checkLogin($rootScope,$scope.$storage.username,$location,true,"/addcam");
	$rootScope.loginSuccess = false;
	$rootScope.backbutton = false;

	$scope.loginAuthencation = function() {
		$http({
			method: 'post',
			url: 'http://52.58.40.46/authenticate',
			data: {
				"username": $scope.username,
				"password": $scope.password
			}
		}).success(function(response) {
			if (response.status === "success") {
				$scope.$storage.username = response.username;
				$rootScope.loginSuccess = true;
				$http({
					method: 'get',
					url: 'http://52.58.40.46/getUserId?useremail=' + $scope.$storage.username
				}).success(function(response) {
					$scope.$storage.userid = response.id;
					$location.path("/addcam");
				});				
			} else {
				$("#error").html(response.status).show();
				$rootScope.loginSuccess = false;
				$location.path("/");
			}
		});
	}
	
	$scope.logout = function(){
		$sessionStorage.$reset();		
		$location.path("/");
		$("#fullPage").click();
	}
}]);

app.controller("scanCtrl", ["$scope", "$rootScope", "$http", "$sessionStorage","$location", function($scope, $rootScope, $http, $sessionStorage,$location) {	
	$scope.$storage = $sessionStorage;
	checkLogin($rootScope,$scope.$storage.username,$location,true,"/addcam");
	$rootScope.loginSuccess = true;
	$rootScope.backbutton = false;
	$scope.wifiConnected = false;
	$scope.scanResults = [];
	$scope.addCamera = function() {
		$http({
			method: 'post',
			url: 'http://52.58.40.46/addCamera',
			data: {
				"cameraid": $scope.cameraid,
				"userid": $scope.$storage.userid,
			}
		}).success(function(response) {
			$("#status").html(response.status).show();
			$scope.myCameras();
		});
	};
	$scope.test = function(){
		WifiWizard.isWifiEnabled(function(status){
			console.log(status);
			if(!status){				
				WifiWizard.setWifiEnabled(true,function(){
					$scope.wifiConnected = true;
				});				
			}else{
				$scope.wifiConnected = true;
			}
	
			WifiWizard.getScanResults(function(scanresults){						
				$scope.scanResults = scanresults;
				console.log(scanresults[0]);
				console.log(scanresults[0].BSSID);
				console.log(scanresults[0].SSID);
			});				
		});
	}
	$scope.myCameras = function() {
		$http({
			method: 'get',
			url: 'http://52.58.40.46/myCameras?id=' + $scope.$storage.userid,
			data: {
				"cameraid": $scope.cameraid,
				"userid": $scope.$storage.userid,
			}
		}).success(function(response) {
			console.log(response.length)
			var html = '';
			$(response).each(function(i,e) {
				
				if(response.length>0){
					html += '<div><a href="#/camview" data-cameraid="'+e.cameraid+'"><img src="img/screen.jpg" alt="screen"/></a><label>'+e.cameraid+'</label></div>'; 
				} else {
					alert();
					html += "<div>No cameras were add, please add.</div>";
				}
			})
			$("#mycameras").html(html);
		});
	}
	$scope.test();
	$scope.myCameras();
}]);
app.controller("regCtrl", ["$rootScope","$scope","$sessionStorage","$location","$http", function($rootScope,$scope,$sessionStorage,$location,$http) {
	$scope.$storage = $sessionStorage;
	checkLogin($rootScope,$scope.$storage.username,$location,false,"/addcam");
	$rootScope.backbutton = true;
	$scope.registerUser = function(){		
		$http({
			method: 'post',
			url: 'http://52.58.40.46/register',
			data: {
				"email": $scope.email,
				"password": $scope.password,
				"mobile": $scope.mobile,
				"country": $scope.country,
			}
		}).success(function(response) {
			$location.path("/");
			$("#error").html(response.status).show();
		});
	}
}]);
app.controller("fpCtrl", ["$rootScope","$scope","$sessionStorage","$location", function($rootScope,$scope,$sessionStorage,$location) {
	$scope.$storage = $sessionStorage;
	checkLogin($rootScope,$scope.$storage.username,$location,false,"/addcam");
	$rootScope.backbutton = true;
}]);

app.controller("camCtrl", ["$rootScope","$scope","$sessionStorage","$location", function($rootScope,$scope,$sessionStorage,$location) {
	$scope.$storage = $sessionStorage;
	checkLogin($rootScope,$scope.$storage.username,$location,true,"/mycameras");
	$rootScope.backbutton = true;
}]);

document.addEventListener("deviceready", function(){
	angular.element(document).ready(function () {
      var domElement = document.getElementById('wrapper');
      angular.bootstrap(domElement, ["onvif"]);
    });
}, false);